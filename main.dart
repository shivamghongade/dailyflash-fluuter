// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.menu), 
          title: Text(
            'My App'
          ), 
          actions: [
            
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                
              },
            ),
          ],
        ),
        body: Center(
          child: Text(
            'hello Here',
            ),
        ),
        
      ),
    );
  }
}
